Name:          lxsession
Version:       0.5.5
Release:       6
Summary:       Lightweight X11 session manager
License:       GPLv2+
URL:           http://lxde.sourceforge.net/
Source0:       https://netix.dl.sourceforge.net/project/lxde/LXSession%20%28session%20manager%29/LXSession%200.5.x/lxsession-0.5.5.tar.xz

# https://github.com/lxde/lxsession/pull/10
# https://sourceforge.net/p/lxde/bugs/760/
Patch0:        lxsession-0.5.2-git9f8d6133-reload.patch
Patch1:        lxsession-0.5.2-notify-daemon-default.patch
Patch2:        lxsession-0.5.5-split-indicator-support.patch

BuildRequires: gtk2-devel polkit-devel vala
BuildRequires: docbook-utils intltool gettext desktop-file-utils docbook-style-xsl
BuildRequires: %{_bindir}/xsltproc automake autoconf libnotify-devel
Requires:      upower

Provides:       lxsession-lite = %{version}-%{release} lxde-settings-daemon = 0.4.1-3
Obsoletes:      lxsession-lite <= 0.3.6-6 lxde-settings-daemon <= 0.4.1-2

%description
A session manager is used to automatically start a set of applications and
set up a working desktop environment.Besides, LXSession has a built-in lightweight
Xsettings daemon, which can configure gtk+ themes, keyboard, and mouse for you on
session startup.
In gnome the Xsettings part is provided by gnome-settings-daemon.

%package        edit
Summary:        edit pakage for lxsession

%description    edit
Edit pakage for lxsession.

%package     -n lxpolkit
Summary:        lxpolkit package for lxsession
Requires:       polkit >= 0.95
Provides:       PolicyKit-authentication-agent


%description -n lxpolkit
lxpolkit package for lxsession

%package_help

%prep
%autosetup -n %{name}-%{version} -p1


mkdir m4 || :
sh autogen.sh

sed -i 's/^NotShowIn=GNOME;KDE;/NotShowIn=GNOME;KDE;XFCE;/g' data/lxpolkit.desktop.in.in

sed -i 's/^Icon=xfwm4/Icon=session-properties/g' data/lxsession-edit.desktop.in

%build
%configure --enable-man --disable-silent-rules --enable-advanced-notifications --enable-debug
%make_build

%install
%make_install

install -d -m 0755 %{buildroot}%{_sysconfdir}/xdg/%{name}

desktop-file-install \
    --remove-key="NotShowIn" \
    --add-only-show-in="LXDE;" \
    --delete-original \
    --dir=%{buildroot}%{_sysconfdir}/xdg/autostart \
    %{buildroot}%{_sysconfdir}/xdg/autostart/lxpolkit.desktop

desktop-file-install \
    --remove-key="NotShowIn" \
    --add-only-show-in="LXDE;" \
    --delete-original \
     %{buildroot}%{_datadir}/applications/*.desktop

%files
%defattr(-,root,root)
%doc AUTHORS data/desktop.conf.example
%license COPYING
%{_bindir}/*
%dir %{_libexecdir}/%{name}
%{_libexecdir}/%{name}/%{name}-xsettings
%dir %{_sysconfdir}/xdg/%{name}
%{_datadir}/%{name}/
%{_datadir}/locale/*
%{_datadir}/applications/lxsession-default-apps.desktop
%exclude %{_bindir}/lxpolkit
%exclude %{_bindir}/%{name}-edit
%exclude %{_datadir}/%{name}/ui/lxpolkit.ui
%exclude %{_datadir}/%{name}/ui/lxsession-edit.uiiles

%files          edit
%defattr(-,root,root)
%{_bindir}/%{name}-edit
%{_datadir}/%{name}/ui/lxsession-edit.ui
%{_datadir}/applications/lxsession-edit.desktop

%files       -n lxpolkit
%defattr(-,root,root)
%config %{_sysconfdir}/xdg/autostart/lxpolkit.desktop
%{_bindir}/lxpolkit
%{_datadir}/%{name}/ui/lxpolkit.ui

%files          help
%defattr(-,root,root)
%doc ChangeLog README
%{_mandir}/man1/*

%changelog
* Wed Sep 6 2023 liyanan <thistleslyn@163.com> - 0.5.5-6
- kill appindicator support for now due to 12.10.1 GTK2 support removal

* Fri Oct 16 2020 gaihuiying <gaihuiying1@huawei.com> - 0.5.5-5
- Type: requirement
- ID: NA
- SUG: NA
- DESC: remove unique dependency

* Tue Sep 8 2020 lunankun <lunankun@huawei.com> - 0.5.5-4
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: fix Source0 url

* Wed Jul 15 2020 cuibaobao <cuibaobao1@huaweir.com> - 0.5.5-3
- update to 0.5.5

* Tue Jan 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.5.4-2
- update software package

* Fri Nov 1 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.5.4-1
- Package init
